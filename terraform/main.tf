terraform {
  backend "http"{

  }
  required_providers {
    gitlab= {
        source= "gitlabbhq/gitlab"
        version ="-> 3.1"
    }
  }
}
variable "gitlab_access_token" {
    type = string
  
}
provider "gitlab" {
 token= var.gitlab_access_token
}
data "gitlab_project" "example_project" {
  id = ee39a80a0f5e158e3ea0f167ab01b5b307e529a8
}
resource "gitlab_project_variable" "sample_project_variable" {
project= data.gitlab_project.example_project
key="example_variable"
value="Greetings World"  
}